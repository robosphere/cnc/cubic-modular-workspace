# Модуль с интегрированным стыковочным узлом

![cubic-modular-workspace](img/surf_samp_v05.png)

## Особенности

- Единое платформенное решение не требующее дополнительных модулей для образования рабочего пространства;
- При любой конфигурации не нарушается пространственная координатная сетка;
- Сокращено количество деталей в конструкции.

К недостаткам конструкции относится невозможность демонтажа угловых блоков без разбора соседних элементов.

## Вариант исполнения со стыковочным интерфейсом

![cubic-modular-workspace](img/connector_plate_v05.png)

## Вариант исполнения с заглушкой

![cubic-modular-workspace](img/connector_empty_plate_v05.png)

## Прототип

![cubic-modular-workspace](img/connector_empty_plate_printed_v05.png)

## Предыдущие версии

### Версия 04.1
![cubic-modular-workspace](img/connector_plate_v04_1.png)
![cubic-modular-workspace](img/empty_plate_v04_1.png)

### Версия 04

![cubic-modular-workspace](img/plate_empty_element_v04.png)

### Версия 03

![cubic-modular-workspace](img/plate_model_v03.png)

![cubic-modular-workspace](img/plate_element_v03.png)

![cubic-modular-workspace](img/plate_empty_element_v03.png)

![cubic-modular-workspace](img/plate_link_v03.png)

### Версия 02

![cubic-modular-workspace](img/cube_v02.png)

### Версия 01

![cubic-modular-workspace](img/cube_v01.png)
